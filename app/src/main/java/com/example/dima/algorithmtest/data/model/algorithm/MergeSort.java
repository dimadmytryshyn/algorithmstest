package com.example.dima.algorithmtest.data.model.algorithm;

public class MergeSort extends Algorithm {

    public MergeSort() {
        super("Merge sort");
    }

    //region Algorithm
    @Override
    public void sort(int[] array) {
        mergeSort(array, 0, array.length - 1);
    }
    //endregion

    //region UtilityApi
    private void mergeSort(int[] array, int startIndex, int endIndex) {
        if (startIndex < endIndex) {
            int middleIndex = (startIndex + endIndex) / 2;
            mergeSort(array, startIndex, middleIndex);
            mergeSort(array, middleIndex + 1, endIndex);
            merge(array, startIndex, middleIndex, endIndex);
        }
    }

    private void merge(int[] array, int startIndex, int middleIndex, int endIndex) {
        int[] helper = new int[array.length];
        System.arraycopy(array, startIndex, helper, startIndex, endIndex + 1 - startIndex);

        int helperStartIndex = startIndex;
        int helperEndIndex = middleIndex + 1;
        int currentIndex = startIndex;

        while (helperStartIndex <= middleIndex && helperEndIndex <= endIndex) {
            if (helper[helperStartIndex] <= helper[helperEndIndex]) {
                array[currentIndex] = helper[helperStartIndex];
                helperStartIndex++;
            } else {
                array[currentIndex] = helper[helperEndIndex];
                helperEndIndex++;
            }
            currentIndex++;
        }

        int remaining = middleIndex - helperStartIndex;
        System.arraycopy(helper, helperStartIndex, array, currentIndex, remaining + 1);
    }
    //endregion
}
