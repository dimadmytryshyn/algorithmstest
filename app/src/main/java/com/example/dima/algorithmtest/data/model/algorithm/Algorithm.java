package com.example.dima.algorithmtest.data.model.algorithm;

import android.support.annotation.NonNull;

public abstract class Algorithm {

    @NonNull
    private String name;

    Algorithm(@NonNull String name) {
        this.name = name;
    }

    public abstract void sort(int[] array);

    //region Getter
    @NonNull
    public String getName() {
         return this.name;
    }
    //endregion

    //region Object
    @Override
    public String toString() {
        return this.name;
    }
    //endregion
}
