package com.example.dima.algorithmtest.data.model.algorithm;

public class QuickSort extends Algorithm {

    public QuickSort() {
        super("Quick sort");
    }

    //region Algorithm
    @Override
    public void sort(int[] array) {
        doSort(array, 0, array.length - 1);
    }
    //endregion

    //region UtilityApi
    private void doSort(int[] array, int startIndex, int endIndex) {
        if (startIndex >= endIndex) {
            return;
        }
        int pivot = startIndex - (startIndex - endIndex) / 2;
        while (startIndex < endIndex) {
            while (startIndex < pivot && (array[startIndex] <= array[pivot])) {
                startIndex++;
            }
            while (endIndex > pivot && (array[pivot] <= array[endIndex])) {
                endIndex--;
            }
            if (startIndex < endIndex) {
                int temp = array[startIndex];
                array[startIndex] = array[endIndex];
                array[endIndex] = temp;
                if (startIndex == pivot)
                    pivot = endIndex;
                else if (endIndex == pivot)
                    pivot = startIndex;
            }
        }
        doSort(array, startIndex, pivot);
        doSort(array, pivot + 1, endIndex);
    }
    //endregion
}
