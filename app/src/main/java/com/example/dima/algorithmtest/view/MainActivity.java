package com.example.dima.algorithmtest.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.dima.algorithmtest.R;
import com.example.dima.algorithmtest.data.model.algorithm.Algorithm;
import com.example.dima.algorithmtest.data.model.algorithm.BubbleSort;
import com.example.dima.algorithmtest.data.model.algorithm.MergeSort;
import com.example.dima.algorithmtest.data.model.algorithm.QuickSort;
import com.example.dima.algorithmtest.view.adapter.AlgorithmArrayAdapter;
import com.example.dima.algorithmtest.view.adapter.AlgorithmResultRecyclerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    //region Constants
    private static final String TAG = "MainActivity";
    //endregion

    //region Views
    @BindView(R.id.mainActivityAlgorithmSpinner)
    Spinner chooseAlgorithmSpinner;

    @BindView(R.id.mainActivityCapacityEditText)
    EditText capacityEditText;

    @BindView(R.id.mainActivityRecyclerViewResults)
    RecyclerView resultRecyclerView;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initView();
    }

    //region ButterKnife
    @OnClick(R.id.mainActivityButtonSubmit)
    public void OnClick(View v) {
        String sortResult = sortArray();
        updateRecyclerView(sortResult);
    }
    //endregion

    //region UtilityApi
    private void initView() {
        initSpinner();
        initRecyclerView();
    }

    private void initRecyclerView() {
        resultRecyclerView.setAdapter(new AlgorithmResultRecyclerAdapter(new ArrayList<String>()));
    }

    private void initSpinner() {
        chooseAlgorithmSpinner.setAdapter(new AlgorithmArrayAdapter(this,
                android.R.layout.simple_spinner_dropdown_item,
                new Algorithm[]{new BubbleSort(), new MergeSort(), new QuickSort()}));
    }

    private void updateRecyclerView(@NonNull String sortResult) {
        ((AlgorithmResultRecyclerAdapter) resultRecyclerView.getAdapter())
                .setSortResult(sortResult);
        resultRecyclerView.scrollToPosition(resultRecyclerView.getAdapter().getItemCount() - 1);
    }

    @NonNull
    private String sortArray() {
        Algorithm algorithm = (Algorithm) chooseAlgorithmSpinner.getSelectedItem();
        int arrayLength = Integer.parseInt(capacityEditText.getText().toString());
        return getString(R.string.sortResult, algorithm.getName(),
                arrayLength, sort(algorithm, generateIntArray(arrayLength)));
    }

    private long sort(Algorithm algorithm, int[] array) {
        long startTime = System.currentTimeMillis();
        algorithm.sort(array);
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    private int[] generateIntArray(int length) {
        Random random = new Random();
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(Integer.MAX_VALUE);
        }
        return array;
    }
    //endregion
}
