package com.example.dima.algorithmtest.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dima.algorithmtest.data.model.algorithm.Algorithm;

public class AlgorithmArrayAdapter extends ArrayAdapter<Algorithm> {

    @NonNull
    private Algorithm[] items;

    public AlgorithmArrayAdapter(@NonNull Context context, int resource, @NonNull Algorithm[] items) {
        super(context, resource, items);
        this.items = items;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setText(items[position].getName());
        return label;
    }
}
