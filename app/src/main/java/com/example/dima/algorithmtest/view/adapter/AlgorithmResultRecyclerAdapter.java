package com.example.dima.algorithmtest.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AlgorithmResultRecyclerAdapter extends
        RecyclerView.Adapter<AlgorithmResultRecyclerAdapter.ViewHolder> {

    @NonNull
    private List<String> sortResult;

    public AlgorithmResultRecyclerAdapter(@NonNull List<String> sortResult) {
        this.sortResult = sortResult;
    }

    public void setSortResult(@NonNull String sortResult) {
        this.sortResult.add(sortResult);
        notifyDataSetChanged();
    }

    //region RecyclerView.Adapter<AlgorithmResultRecyclerAdapter.ViewHolder>
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(android.R.layout.simple_list_item_1, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.resultTextView.setText(sortResult.get(position));
    }

    @Override
    public int getItemCount() {
        return sortResult.size();
    }
    //endregion

    //region AlgorithmResultRecyclerAdapter.ViewHolder
    static class ViewHolder extends RecyclerView.ViewHolder {

        @NonNull
        TextView resultTextView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            resultTextView = itemView.findViewById(android.R.id.text1);
        }
    }
    //endregion
}
