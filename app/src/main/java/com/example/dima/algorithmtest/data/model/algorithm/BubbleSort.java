package com.example.dima.algorithmtest.data.model.algorithm;

public class BubbleSort extends Algorithm {

    public BubbleSort() {
        super("Bubble sort");
    }

    //region Algorithm
    @Override
    public void sort(int[] array) {
        doSort(array);
    }
    //endregion

    //region UtilityApi
    private void doSort(int[] array) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    isSorted = false;
                }
            }
        }
    }
    //endregion
}
